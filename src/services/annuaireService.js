import axios from 'axios'

const API_URL = process.env.API_URL || 'https://etablissements-publics.api.gouv.fr/v3/departements/'
const apiClient = axios.create({
    baseURL: API_URL,
    withCredentials: false,
})


export default {
    async getEntreprises(qpv) {
        let response = await apiClient.get("https://prod-16.francecentral.logic.azure.com/workflows/9d0e6912ed514420aa88821286826985/triggers/manual/paths/invoke/entreprises/qp/"+qpv+"?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=DoJe84KAkNxFuteWr3fML1ajhvH2vlpaDndoUMyC9r0")

        return response.data.value;

    },

    async getInfoQpv(qpv) {
        let response = await apiClient.get("https://prod-21.francecentral.logic.azure.com/workflows/6a13b04796724bf4b7ebb0094ca80fdc/triggers/manual/paths/invoke/quartier/"+qpv+"?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=JFnAqME7jbGcchWinXDaGpd25PpXQuzfmMwdMK7OpLQ")

        return response.data.value;

    },

    async getQpvBySiren(siren) {
        let response = await apiClient.get("https://prod-01.francecentral.logic.azure.com/workflows/0685c1ea6e0f442ab55fe732cf768cfa/triggers/manual/paths/invoke/entreprises/siren/qpv/"+siren+"?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=c-1jp0xtz3GWebdiR_OCCSS92brkakslvMBIyDDXysI ")

        return response.data.value;

    },
}